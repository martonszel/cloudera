import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Repository } from 'src/app/repo';
import { HttpClient } from '@angular/common/http';

const GITHUB_URL = 'https://api.github.com/repos/twbs/bootstrap/issues';

@Component({
  selector: 'app-github-issues',
  templateUrl: './github-issues.component.html',
  styleUrls: ['./github-issues.component.scss']
})
export class GithubIssuesComponent implements OnInit {


  constructor(private http: HttpClient) {
  }

  ngOnInit() {
  }

}
