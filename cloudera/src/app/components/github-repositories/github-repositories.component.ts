import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/';
import { Repository } from 'src/app/repo';
import { HttpClient } from '@angular/common/http';

const GITHUB_URL = 'https://api.github.com/search/repositories';

@Component({
  selector: 'app-github-repositories',
  templateUrl: './github-repositories.component.html',
  styleUrls: ['./github-repositories.component.scss']
})
export class GithubRepositoriesComponent implements OnInit {

  searchResult$: Observable<Repository[]>;
  page = 1;
  pageSize = 10;

  constructor(private http: HttpClient) {
  }

  onTextChange(query: string) {
      this.searchResult$ = this.fetchRepositories(query);
      localStorage.setItem('searchresult', JSON.stringify(this.searchResult$));
  }

  private fetchRepositories(query: string): Observable<Repository[]> {
      const params = { q: query };
      return this.http.get<Repository[]>(GITHUB_URL, { params });
  }

  /*
  detailRepositories(id: number) {
  this.searchResult$.subscribe(data => {
     data.items.find(
      e => e.id === id
    )
    })
}
*/

  ngOnInit() {
  }


}
