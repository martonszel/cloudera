import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { GithubRepositoriesComponent } from './components/github-repositories/github-repositories.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { GithubIssuesComponent } from './components/github-issues/github-issues.component';


@NgModule({
  declarations: [
    AppComponent,
    GithubRepositoriesComponent,
    GithubIssuesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
