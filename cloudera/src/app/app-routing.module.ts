import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GithubRepositoriesComponent } from './components/github-repositories/github-repositories.component';
import { GithubIssuesComponent } from './components/github-issues/github-issues.component';


const routes: Routes = [
  { path: '', component: GithubRepositoriesComponent },
  { path: 'issues/:id', component: GithubIssuesComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
